const cv = require('opencv4nodejs')
console.log('Create Frame Processor')
exports.grabFrames = (videoFile, delay, onFrame, onDone) => {
  const cap = new cv.VideoCapture(videoFile);
  let done = false;
  const intvl = setInterval(() => {
    let frame = cap.read();

    if (frame.empty) {
        done = true
        clearInterval(intvl);
        onDone();
        return
    }
    onFrame(frame);

    const key = cv.waitKey(delay);
    done = key !== -1 && key !== 255;
    if (done) {
      clearInterval(intvl);
    }
  }, 0);
};

exports.newFrameChecker = (options) => {
    options = options ? options : {}
    let skipFrames = options.skipFrames || 10
    let minimumArea = options.minimumArea || 2000
    let lastFrame = null
    let newestFrame = null
    let framesSkipped = 0
    const checkFrame = (frame) => {
        let motionFound = [];
        return new Promise((resolve, reject) => {

            framesSkipped += 1

            if(framesSkipped > skipFrames){
                framesSkipped = 0
                lastFrame = newestFrame || lastFrame
            }else{
                return
            }
            let gray = frame.cvtColor(cv.COLOR_BGR2GRAY)
            // gray = gray.gaussianBlur(new cv.Size(21, 21), 0)
            if(!lastFrame)lastFrame = gray;
            newestFrame = gray


            const frameDifference = lastFrame.absdiff(newestFrame)
            let thresh = frameDifference.threshold(25, 255, cv.THRESH_BINARY)

            thresh = thresh.dilate(cv.getStructuringElement(cv.MORPH_ELLIPSE, new cv.Size(4, 4)),new cv.Point(-1, -1), 2)
            const cnts = thresh.copy().findContours(cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)

            cnts.forEach((c) => {
                const matrix = c.boundingRect()
                if(c.area > minimumArea){
                    motionFound.push(matrix)
                }
            })
            resolve(motionFound)
        })
    }
    return checkFrame
}
