# opencv-motion-detector
## OpenCV Motion Detector for Node.js

- Made by Moe Alam, Shinobi Systems - https://shinobi.video
- Discord : https://shinobi.community

> This module was made to be used in ShinobiCCTV as an extended feature where you may replace the pam-diff Motion Detection engine with an OpenCV based Motion Detection engine. However this module can be used in other projects.

```
//download video from https://cdn.shinobi.video/videos/people.mp4 for testing

const { newFrameChecker, grabFrames} = require('opencv-motion-detector')
const checkFrame = newFrameChecker({
    skipFrames: 10,
    minimumArea: 2000,
})
grabFrames(`./people.mp4`, 10, async (frame) => {
    console.log('New Frame!')
    const matrices = await checkFrame(frame)
    if(matrices.length > 0){
        console.log('Motion Found!')
        console.log(matrices)
    }
}, () => {
    //on done
    console.log('Done Frames')
})

```

Licensed under MIT https://opensource.org/licenses/MIT
