//download video from https://cdn.shinobi.video/videos/people.mp4 for testing

const { newFrameChecker, grabFrames} = require('./index.js')
const checkFrame = newFrameChecker({
    skipFrames: 10,
    minimumArea: 2000,
})
grabFrames(`./people.mp4`, 10, async (frame) => {
    console.log('New Frame!')
    const matrices = await checkFrame(frame)
    if(matrices.length > 0){
        console.log('Motion Found!')
        console.log(matrices)
    }
}, () => {
    //on done
    console.log('Done Frames')
})
